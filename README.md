# Reembolsa.ai json server api.

## !!DISCLAIMER: The purpose of this API is only for study and mocking tests. do not use in production! You have been warned ...


### Base URL


## Base heroku url
``` 
https://reembolsa-ai-api.herokuapp.com/

```



# Endpoints

## Employer
### POST/register

#### body

> You don't need to set the user id in the post request, api does it automatically.
### IMPORTANT!!! remember: the "access-level" key must be implicit in the user registry.

```
{
  "email": "company@company.com",
  "password": "123456",
  "company": "Company",
  "accessLevel": 1,
}
```

## Employee
### POST/register

#### body

> You don't need to set the user id in the post request, api does it automatically

```
{
  "email": "john-doe@company.com",
  "password": "123456",
  "fullName": "John Doe",
  "user": "johnd",
  "accessLevel": 2,
  "company": "Company",
  "roll": "software engineer",
  "amountLimit": 800,
  "id": 2
}
```

### POST/login

#### body

```
{
  "email": "example@example.com",
  "password": "123456"
}
```

### GET/users

#### header

```
Bearer: <TOKEN>
```

### GET /users/id

#### header

```
Bearer: <TOKEN>
```


### POST/refunds

#### body

```
{
  "category": "combustivel",
  "value": 199.9,
  "date": 25022020,
  "description": "lorem ipsum dolor sit ammet",
  "userId": 1,
  "status": "pending",
  "id": 1
}
```

### PUT/refunds/:id

#### body

> Update the refund status to aproved!! I know it's coool!

```
{
  "category": "combustivel",
  "value": 159.9,
  "date": 25022020,
  "description": "lorem ipsum dolor sit ammet",
  "userId": 1,
  "status": "aproved",
  "id": 1
}
```

### GET /refunds

#### header

```
Bearer: <TOKEN>
```

### GET /refunds?keyname_like=keyvalues
> Filtering refunds per entity key using like method.
> You can use other methods like gte or lte to filter refund values.

#### header

```
Bearer: <TOKEN>
```

### GET /users/:id/refunds
> You can use the similarity with REST Api to filter refunds per user like this
> ... Or this:

```
/users/:id/refunds?keyname_like=keyvalue
```

#### header
```
Bearer: <TOKEN>
```

### PATCH /users/:id
> Update amount limit by user.

#### body


```
{
	"amount-limit":800
}
```

